package com.example.lw_ubuntu.parkrapp;
//SEE ATTRIBUTION BELOW. Main App Page. Parent to MapHolderActivity
// Generates buttons for U interaction. Utilizes it's child activity
//as well as fragments(functions(self), timer, mapholder).
//called by: Launch
//accepts: Google location object
//delivers: navigation to above, with context and Google location object

//ATTRIBUTION: this code is adapted from Aaron MacIntyre's mapsmateriallecture app

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.content.Intent;
import android.location.Location;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

/**
 * Created by lw-ubuntu on 5/17/15.
 */
//Displays buttons via MyListAdapter. Listens to clicks from self.frag
//    calls MapHolderActivity and/or MapHolderFragment
public class MainActivity extends ActionBarActivity implements Mfunctions.Callbacks {

    public static final String LOCATION_TAG = "my_location";
    static Context mycontext;

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mycontext = getApplicationContext();

        if (findViewById(R.id.map_holder_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-large and
            // res/values-sw600dp). If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;

            // In two-pane mode, list items should be given the
            // 'activated' state when touched.
            Mfunctions mfunctions = (Mfunctions) getSupportFragmentManager()
                    .findFragmentById(R.id.main);
        }
//                    mParkedLocation = mCurrLocation;//TEMP
//                    carIsParkedFlag = 1;
////                    LatLng is easier to store
//                    carlat = mParkedLocation.getLatitude();
//                    carlon = mParkedLocation.getLongitude();
//                    String latstr = String.valueOf(carlat);
//                    String lonstr = String.valueOf(carlon);
                        String teststr = "testing";
//                    /******* Create SharedPreferences *******/
                    SharedPreferences sharedPref = this.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString("teststring", teststr);  // Saving string
//                    editor.putString("lon1", lonstr);  // Saving string
//                    // Save the changes in SharedPreferences
                    editor.commit(); // commit changes
                    String received;
//                    sharedPref = context.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                    received = sharedPref.getString("teststring", "nope");// getting String
//                    String lonstr2 = String.valueOf(sharedPref.getString("lat2", null));
//                    double carlat = Double.valueOf(latstr2.trim()).doubleValue();
//                    double carlon = Double.valueOf(lonstr2.trim()).doubleValue();
//                    carLatLon = new LatLng(carlat, carlon);
        Toast.makeText(this, "String is " + received, Toast.LENGTH_LONG).show();
    }

    /**
     * Callback method from {@link Mfunctions.Callbacks}
     * indicating that the item with the given ID was selected.
     */
    @Override
    public void onItemSelected(Location location) {
        if (mTwoPane) {
            // In two-pane mode, show the detail view in this activity by
            // adding or replacing the detail fragment using a
            // fragment transaction.
            Bundle arguments = new Bundle();
            arguments.putParcelable(LOCATION_TAG, location);
            MapHolderFragment fragment = new MapHolderFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.map_holder_container, fragment)
                    .commit();

        } else {
            // In single-pane mode, simply start the detail activity
            // for the selected item ID.
            Intent mapIntent = new Intent(this, MapHolderActivity.class);
            mapIntent.putExtra(LOCATION_TAG, location);
            startActivity(mapIntent);
        }
    }

}
