package com.example.lw_ubuntu.parkrapp;
//SEE ATTRIBUTION BELOW. Child of MainActivity
// Activity manages the MapHolderFragment in case that fragment is needed by
//phone handset device. If U.tablet device, MainActivity calls it's fragment instead
//called by: Parent
//accepts: location object
//delivers: location to childfrag

//ATTRIBUTION: this code is adapted from Aaron MacIntyre's mapsmateriallecture app

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.content.Intent;
import android.support.v4.app.NavUtils;

/**
 * Created by lw-ubuntu on 5/17/15.
 */
//Calls its fragment if called upon to do so by main. Passes location object through
public class MapHolderActivity extends ActionBarActivity {

    public Context globalContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_holder);
        globalContext = getApplicationContext();

        // Show the Up button in the action bar.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            Bundle arguments = new Bundle();
//            takes the bundled args from MainActivity and bundles them for frag
            arguments.putParcelable(MainActivity.LOCATION_TAG,
                    getIntent().getParcelableExtra(MainActivity.LOCATION_TAG));
            MapHolderFragment fragment = new MapHolderFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.map_holder_container, fragment)
                    .commit();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            NavUtils.navigateUpTo(this, new Intent(this, MainActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
