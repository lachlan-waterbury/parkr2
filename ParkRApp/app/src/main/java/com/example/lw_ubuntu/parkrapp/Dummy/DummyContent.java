package com.example.lw_ubuntu.parkrapp.Dummy;
//SEE ATTRIBUTION BELOW. Dummy callback stubs so that if a fragment is not attached to an activity,
//onDetach(), app can reset callbacks interface to these (empty)values to avoid a null pointer crash.
//Essentially allows fragment to be a nonop stub instead of a forceclose event

//ATTRIBUTION: this code is copied from Aaron MacIntyre's mapsmateriallecture app

/**
 * Created by lw-ubuntu on 5/17/15.
 */

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 */
public class DummyContent implements Parcelable {

    /**
     * An array of sample (dummy) items.
     */
    public static List<DummyItem> ITEMS = new ArrayList<DummyItem>();

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public int i;

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static Map<String, DummyItem> ITEM_MAP = new HashMap<String, DummyItem>();

    static {
        // Add 3 sample items.
        addItem(new DummyItem("1", "Item 1"));
        addItem(new DummyItem("2", "Item 2"));
        addItem(new DummyItem("3", "Item 3"));
    }

    private static void addItem(DummyItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class DummyItem {
        public String id;
        public String content;

        public DummyItem(String id, String content) {
            this.id = id;
            this.content = content;
        }

        @Override
        public String toString() {
            return content;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.i);
    }

    public DummyContent() {
    }

    private DummyContent(Parcel in) {
        this.i = in.readInt();
    }

    public static final Parcelable.Creator<DummyContent> CREATOR = new Parcelable.Creator<DummyContent>() {
        public DummyContent createFromParcel(Parcel source) {
            return new DummyContent(source);
        }

        public DummyContent[] newArray(int size) {
            return new DummyContent[size];
        }
    };
}
