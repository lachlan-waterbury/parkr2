package com.example.lw_ubuntu.parkrapp;
//fragment to manage the original ParkR timer code.

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.app.ActionBar;
import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.util.TimerTask;
import javax.security.auth.callback.Callback;

/**
 * Created by mbramirez on 5/7/15.
 */
public class Timer extends android.support.v4.app.Fragment implements View.OnClickListener{

    TextView editText;
    private Integer Meter_Time = 0 ;
    EditText timer_time;

    boolean isCounterRunning = false;

    Button one, two, three, four, five, six, seven, eight, nine, backspace, zero, start;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public Timer() {
    }

//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View v =inflater.inflate(R.layout.tab_2,container,false);
//        return v;
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_timer,container,false);

        timer_time = (EditText) v.findViewById(R.id.editText2);

        editText = (TextView) v.findViewById(R.id.editText);

        one = (Button) v.findViewById(R.id.one);
        two = (Button) v.findViewById(R.id.two);
        three = (Button) v.findViewById(R.id.three);
        four = (Button) v.findViewById(R.id.four);
        five = (Button) v.findViewById(R.id.five);
        six = (Button) v.findViewById(R.id.six);
        seven = (Button) v.findViewById(R.id.seven);
        eight = (Button) v.findViewById(R.id.eight);
        nine = (Button) v.findViewById(R.id.nine);
        backspace = (Button) v.findViewById(R.id.backspace);
        zero = (Button) v.findViewById(R.id.zero);
        start = (Button) v.findViewById(R.id.start);

        try{
            one.setOnClickListener(this);
            two.setOnClickListener(this);
            three.setOnClickListener(this);
            four.setOnClickListener(this);
            five.setOnClickListener(this);
            six.setOnClickListener(this);
            seven.setOnClickListener(this);
            eight.setOnClickListener(this);
            nine.setOnClickListener(this);
            backspace.setOnClickListener(this);
            zero.setOnClickListener(this);
            start.setOnClickListener(this);
        } catch(Exception e) {
        }

        return v;
    }

    @Override
    public void onClick(View v) {

        Editable val = timer_time.getText();

        switch(v.getId()) {
            case R.id.one:
                val = val.append(one.getText());
                timer_time.setText(val);
                break;
            case R.id.two:
                val = val.append(two.getText());
                timer_time.setText(val);
                break;
            case R.id.three:
                val = val.append(three.getText());
                timer_time.setText(val);
                break;
            case R.id.four:
                val = val.append(four.getText());
                timer_time.setText(val);
                break;
            case R.id.five:
                val = val.append(five.getText());
                timer_time.setText(val);
                break;
            case R.id.six:
                val = val.append(six.getText());
                timer_time.setText(val);
                break;
            case R.id.seven:
                val = val.append(seven.getText());
                timer_time.setText(val);
                break;
            case R.id.eight:
                val = val.append(eight.getText());
                timer_time.setText(val);
                break;
            case R.id.backspace:
                timer_time.setText("");
                break;
            case R.id.zero:
                val = val.append(zero.getText());
                timer_time.setText(val);
                break;
            case R.id.start:

                if (isCounterRunning == false) {

                    String str = timer_time.getText().toString();

                    try {
                        Meter_Time = Integer.parseInt(str) * 60000;
                    } catch (NumberFormatException e) {
                        Toast.makeText(getActivity(), "Wrong Number Format", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    Toast.makeText(getActivity(), "Timer Set", Toast.LENGTH_SHORT).show();
                    isCounterRunning = true;

                    final CountDownTimer timer = new CountDownTimer(Meter_Time, 1000) {

                        @Override
                        public void onTick(long millisUntilFinished) {
                            editText.setText(String.valueOf((millisUntilFinished / 3600000 < 10 ? "0" +
                                    millisUntilFinished / 3600000 : millisUntilFinished / 3600000) + ":" +
                                    String.valueOf(((millisUntilFinished / 60000) % 60) < 10 ? "0" +
                                            ((millisUntilFinished / 60000) % 60) : ((millisUntilFinished / 60000) % 60)) + ":" +
                                    String.valueOf(((millisUntilFinished / 1000) % 60) < 10 ? "0" +
                                            ((millisUntilFinished / 1000) % 60) : ((millisUntilFinished / 1000) % 60))));
                        }

                        public void onFinish() {
                            editText.setText("RUN!!");
                            isCounterRunning = false;
                        }
                    }.start();
                }
                break;
        }
    }
}