package com.example.lw_ubuntu.parkrapp;
//SEE ATTRIBUTION BELOW. Fragment inflates the map using location provided
//called by: Parent OR MainActivity
//accepts: location object
//delivers: map
//ATTRIBUTION: this code is adapted from Aaron MacIntyre's mapsmateriallecture app

import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.location.Location;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;
import com.example.lw_ubuntu.parkrapp.Dummy.DummyContent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import android.content.Context;

/**
 * A fragment representing a single map screen.
 * This fragment is either contained in a {@link MainActivity}
 * in two-pane mode (on tablets) or a {@link MapHolderActivity}
 * on handsets.
 */
public class MapHolderFragment extends Fragment {

    private SupportMapFragment mapFragment;
    private GoogleMap map;
    private Location currentLoc;
    LatLng carLatLon;
    static Context thisContext;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public MapHolderFragment() {
    }

//    I assume the reason a fragment would have BOTH an oncreate and oncreateview would be
//    because this fragment might be called by an activity(Main) other than its parent?
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Toast.makeText(getActivity(), "fragonCreateView", Toast.LENGTH_LONG).show();
        View rootView = inflater.inflate(R.layout.fragment_map_holder, container, false);
//        get the arguments passed by main activity and grab the location out of them
        Bundle extras = getArguments();
        currentLoc = extras.getParcelable(MainActivity.LOCATION_TAG);
//        if parked, get location out of shared prefs

        Toast.makeText(getActivity(), "Tap the marker to start navigation", Toast.LENGTH_LONG).show();
        return rootView;
    }

    @Override
    public void onResume() {
        Toast.makeText(getActivity(), "fragonResume()", Toast.LENGTH_LONG).show();
        super.onResume();
        if(map == null) {
            map = mapFragment.getMap();
            setLocation();
        }
    }

//    called by onResume and always adds marker to current location
    private void setLocation() {
        Toast.makeText(getActivity(), "fragsetLocation()", Toast.LENGTH_LONG).show();
        map.clear();
        map.setMyLocationEnabled(true);
        LatLng latLng = new LatLng(currentLoc.getLatitude(), currentLoc.getLongitude());
//        if parked, set a marker at car location;
        map.addMarker(new MarkerOptions().position(latLng));
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
        map.animateCamera(cameraUpdate);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Toast.makeText(getActivity(), "fragonActivityCreated", Toast.LENGTH_LONG).show();
        super.onActivityCreated(savedInstanceState);
        FragmentManager fm = getChildFragmentManager();
        mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
        if(mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.map, mapFragment).commit();
        }
    }

}
