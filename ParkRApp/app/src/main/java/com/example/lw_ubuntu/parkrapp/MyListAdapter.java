package com.example.lw_ubuntu.parkrapp;
//SEE ATTRIBUTION BELOW. This package java class formats and manages a list of buttons
//called by: FunctionActivity
//accepts: Google location object
//delivers: layout inflated by context and location

//ATTRIBUTION: this code is adapted from Aaron MacIntyre's mapsmateriallecture app

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by lw-ubuntu on 5/17/15.
 */
//takes a number of google location items and sets each to at set of three textviews
//which act as a single clickable button
public class MyListAdapter extends RecyclerView.Adapter<MyListAdapter.ViewHolder>{
    private OnItemClickListener mOnItemClickListener;
    private final LayoutInflater mInflater;
    static Context context;
//    Array of Google Location items. Needed to bind locaton lat/lon to button appearance
    private ArrayList<Location> mData = new ArrayList<>();

    public MyListAdapter(Context context, OnItemClickListener mOnItemClickListener) {
        mInflater = LayoutInflater.from(context);
        this.mOnItemClickListener = mOnItemClickListener;
    }

    public static interface OnItemClickListener {
        public void onItemClick(Location loc);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public String buttonname = "Tap coordinates to find your car!";
        public TextView mFindMe;
        public TextView mLatView;
        public TextView mLonView;
        private Location location;

        public ViewHolder(View v) {
            super(v);
            mLatView = (TextView) v.findViewById(R.id.lat);
            mLonView = (TextView) v.findViewById(R.id.lon);
            mFindMe = (TextView) v.findViewById(R.id.findme);
            v.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(location);
                    }
                }
            });
        }

        public void bind(Location loc) {
            location = loc;
//            String buttonname = "Show the way!";
            mLatView.setText(String.valueOf(loc.getLatitude()));
            mLonView.setText(String.valueOf(loc.getLongitude()));
            mFindMe.setText(buttonname);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(mInflater.inflate(R.layout.find_button, parent, false));
    }

    @Override
    public void onBindViewHolder(MyListAdapter.ViewHolder holder, int position) {
        holder.bind(mData.get(position));
    }

    public void addData(Location loc) {
        mData.add(loc);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


}
