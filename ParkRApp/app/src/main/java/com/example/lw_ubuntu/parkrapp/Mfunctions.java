package com.example.lw_ubuntu.parkrapp;
//SEE ATTRIBUTION BELOW. Fragment launches GoogleAPI client and gets currentlocation
//called by: Parent
//accepts: may accept stored location object
//delivers: context and Google location object

//ATTRIBUTION: this code is adapted from Aaron MacIntyre's mapsmateriallecture app
//ATTRIBUTION: this code is adapted from Google Maps offical demo app

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.app.Activity;
import android.location.Location;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.melnykov.fab.FloatingActionButton;
import android.content.Context;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

/**
 * A placeholder fragment containing a simple view.
 */
//Prepares for and instanciates GoogleservicesAPI. Gets currentlocation via device GPS
public class Mfunctions extends Fragment
                        implements GoogleApiClient.ConnectionCallbacks,
                                    GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = Mfunctions.class.getSimpleName();
    private static Context context;

    private RecyclerView mRecyclerView;
    private MyListAdapter mListAdapter = null;
    private Location mCurrLocation;
    private Location mParkedLocation;

//    -1 for error checking
    int carIsParkedFlag = 0;
    double carlat = 0;
    double carlon = 0;
    LatLng carLatLon;

    /**
     * Provides the entry point to Google Play services.
     */
    protected GoogleApiClient mGoogleApiClient;
    /**
     * The serialization (saved instance state) Bundle key representing the
     * activated item position. Only used on tablets.
     */
    private static final String STATE_ACTIVATED_POSITION = "activated_position";

    /**
     * The fragment's current callback object, which is notified of list item
     * clicks.
     */
    private Callbacks mCallbacks = sDummyCallbacks;
    private FloatingActionButton fab;

    private boolean mIsConnected = false;

    /**
     * The current activated item position. Only used on tablets.
     */
    private int mActivatedPosition = ListView.INVALID_POSITION;

//    the last call of the app loading process, before any U interaction
//    also the last call after REloading process, ie returning from Directions app
//    If device is not connected to googleAPI, no "park here" button will be displayed
    @Override
    public void onConnected(Bundle bundle) {
        fab.setVisibility(View.VISIBLE);
        mIsConnected = true;
        Toast.makeText(getActivity(),"onConnected", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onConnectionSuspended(int i) {
        fab.setVisibility(View.INVISIBLE);
        mIsConnected = false;
        Toast.makeText(getActivity(),"You are no longer connected to google", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface Callbacks {
        /**
         * Callback for when an item has been selected.
         */
        public void onItemSelected(Location location);
    }

    /**
     * A dummy implementation of the {@link Callbacks} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private static Callbacks sDummyCallbacks = new Callbacks() {
        @Override
        public void onItemSelected(Location location) {
        }
    };

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public Mfunctions() {
    }

//    called second, after onAttach
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        Toast.makeText(getActivity(),"onCreateView", Toast.LENGTH_LONG).show();
        View rootView = inflater.inflate(R.layout.list_main, container, false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerview);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(getAdapter());
//        first call to build googleapiclient
        buildGoogleApiClient();
//        When the button is pushed, calls getLastLocation
        fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
//            first call to getLastLocation
            public void onClick(View v) {
                getLastLocation();
            }
        });

        return rootView;
    }

//    never called until U pushes park button
//    gets last known location from google location services
//    and if car is not parked yet, store it
    private void getLastLocation() {
        Toast.makeText(getActivity(),"getLastLocation", Toast.LENGTH_LONG).show();
        if(mIsConnected) {
            mCurrLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if(mCurrLocation != null) {
                mListAdapter.addData(mCurrLocation);
                mListAdapter.notifyDataSetChanged();
                if(carIsParkedFlag == 0) {
                    Toast.makeText(getActivity(), "carIsParkedFlag == 0", Toast.LENGTH_SHORT).show();
//                    save your location
//                    mParkedLocation = mCurrLocation;//TEMP
//                    carIsParkedFlag = 1;
////                    LatLng is easier to store
//                    carlat = mParkedLocation.getLatitude();
//                    carlon = mParkedLocation.getLongitude();
//                    String latstr = String.valueOf(carlat);
//                    String lonstr = String.valueOf(carlon);
//                    /******* Create SharedPreferences *******/
//                    SharedPreferences sharedPref = getActivity().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
//                    SharedPreferences.Editor editor = sharedPref.edit();
//                    editor.putString("lat1", latstr);  // Saving string
//                    editor.putString("lon1", lonstr);  // Saving string
//                    // Save the changes in SharedPreferences
//                    editor.commit(); // commit changes
//                    sharedPref = context.getSharedPreferences("MyPref", Context.MODE_PRIVATE);
//                    String latstr2 = sharedPref.getString("lat1", null);// getting String
//                    String lonstr2 = String.valueOf(sharedPref.getString("lat2", null));
//                    double carlat = Double.valueOf(latstr2.trim()).doubleValue();
//                    double carlon = Double.valueOf(lonstr2.trim()).doubleValue();
//                    carLatLon = new LatLng(carlat, carlon);

                } else {
//                    "car is already parked. Reset?"
//                    some way to reset()
                }
            }
        }

    }

//    called from onCreateView
//    builds the apiclient
    protected synchronized void buildGoogleApiClient() {
        Toast.makeText(getActivity(),"buildGoogleApiClient", Toast.LENGTH_LONG).show();
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private RecyclerView.Adapter getAdapter() {
        if(mListAdapter == null) {
            mListAdapter = new MyListAdapter(getActivity(), new MyListAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(Location loc) {
                    mCallbacks.onItemSelected(loc);
                }
            });
        }
        return mListAdapter;
    }

//    called when app first loads
//    refers to fragment being attached to activity
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Toast.makeText(getActivity(),"onAttach", Toast.LENGTH_LONG).show();
        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException("Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks) activity;
    }

//    called when home button is pushed from inside map view
//    refers to fragment being detached from its activity
    @Override
    public void onDetach() {
        super.onDetach();
        Toast.makeText(getActivity(),"onDetach", Toast.LENGTH_LONG).show();
        // Reset the active callbacks interface to the dummy implementation.
        mCallbacks = sDummyCallbacks;
    }

//    called when some other app is awakened, i.e. going to Directions app OR
//    called when "find car button" pushed
//    TODO the above line probalby indicates a bug
    @Override
    public void onSaveInstanceState(Bundle outState) {
        Toast.makeText(getActivity(),"onSaveInstanceState", Toast.LENGTH_LONG).show();
        super.onSaveInstanceState(outState);
        if (mActivatedPosition != ListView.INVALID_POSITION) {
            // Serialize and persist the activated item position.
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
    }

//    called automatically after buildGoogleApiClient OR upon returning to app from other app
//    connect to the googleAPIClient
    @Override
    public void onStart() {
        Toast.makeText(getActivity()," onStart", Toast.LENGTH_LONG).show();
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

//    last function called, right after onsavedinstancestate
//    diconnect from the googleAPIClient
    @Override
    public void onStop() {
        Toast.makeText(getActivity()," onStop", Toast.LENGTH_LONG).show();
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

}
